# pastel-night

Pastel Night: A dark pastel colour scheme, inspired by Catppuccin.

This is very much WIP, but the main goals of this project are:

1. Ease of use: Not just creating a palette, but also making sure that this colour scheme can be installed on a number of platforms.
2. Accessibility: Ensuring readability and contrast on all brightness levels.